import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { __core_private_testing_placeholder__ } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { CadastroPage } from './cadastro/cadastro.page';
import { PagarPageRoutingModule } from './pagar/pagar-routing.module';
import { PagarPage } from './pagar/pagar.page';
import { ReceberPage } from './receber/receber.page';
import { RelatorioPage } from './relatorio/relatorio.page';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'pagar',
        component: PagarPage,
      },
      {
        path: 'receber',
        component: ReceberPage,
      },
      {
        path: 'cadastro',
        component: CadastroPage,
      },
      {
        path: 'relatorio',
        component: RelatorioPage,
      },
    ],
  },
];

@NgModule({
  imports: [
    CommonModule,
    IonicModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
  ],
  exports: [RouterModule],
  declarations: [PagarPage, ReceberPage, CadastroPage, RelatorioPage],
})
export class ContasRoutingModule {}
